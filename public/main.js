$(document).ready(function () {

  var $window = $(window);

  // Darken image-overlay when click on the collapsed menu button, only if navbar transparent
  $('.navbar-toggler').on('click', function () {
    if (!$('.navbar-wast').hasClass('navbar-darken')) {
      $('.image-overlay').toggleClass('overlay-darken');
    }
  });

  // Animate title WAST
  if (isScrolledIntoView($('.to-fall'), $window, 0)) {
    setTimeout(function () {
      $('.to-fall').addClass('rotate-and-fall');
    }, 1100);
    setTimeout(function () {
      $('h1').animate({
        left: "+=" + $('h2').width()/2
      }, 800, function() {
        $('h1').css('left', 0);
        $('.to-fall').remove();
      });
    }, 2900)
  }

  $window.on('scroll', function () {

    // Toggle dark navbar if scroll from/to the top
    if ($window.scrollTop() < 100) {
      $('.navbar-wast').removeClass('navbar-darken');
    } else {
      $('.navbar-wast').addClass('navbar-darken');
    }

    // Animate products into view
    if (isScrolledIntoView($('.product'), $window, -100)) {
      $('.product').addClass('move-up');
    }

    if ($('.image-overlay').hasClass('overlay-darken')) {
      $('.navbar-toggler').trigger('click');
    }

  });

  function isScrolledIntoView($elem, $window, offset) {
      var docViewTop = $window.scrollTop();
      var docViewBottom = docViewTop + $window.height();

      var elemTop = $elem.offset().top;
      var elemBottom = elemTop + $elem.height();

      return ((elemBottom <= docViewBottom-offset) && (elemTop >= docViewTop));
  }

});
