$(document).ready(function () {

  $('.close').on('click', function () {
    $('.modal').css('opacity', '0').css('visibility', 'hidden');
  });

  $(document).on('click', function(event) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if (!$(event.target).closest('.btn, .modal-content').length) {
      hideModal();
    }
  }).on('keydown', function (event) { // close the modal if you press 'esc' key
    if (event.keyCode === 27) {
      hideModal();
    }
  });

  // Animate timeline items on viewport
  $('.timeline-item:first-child').addClass('move-up').css('animation-delay', '0.3s');
  $(window).on('scroll', function () {
    $('.timeline-item:not(:first-child)').each(function(index, item) {
      if (isScrolledIntoView($(item), $(window), -100)) {
        $(item).addClass('move-up');
      }
    });
  });
  setTimeout(function() {
    $(window).trigger('scroll');
  }, 100);

  // Timeline buttons halndlers
  $('.button-bicchiere').on('click', function() {
    $('.modal h2').html('Plastic cup');
    $('.modal p').html("Nel 1863 un premio offerto per trovare un sostituto all'avorio porta alla scoperta della celluloide.</br>Pochi anni più tardi i fratelli Hyatt brevettano la prima palla da biliardo in finto avorio, allargando la battaglia per la salvaguardia degli elefanti.</br>Così settant'anni dopo il mondo viene rivoluzionato dall'avvento del PVC, e il successo è clamoroso: la plastica è talmente resistente e immutabile nel tempo che viene astutamente commercializzata come materiale eterno.</br>Ad oggi, però, la plastica è largamente utilizzata per la produzione di prodotti usa e getta: in questo modo un materiale potenzialmente ecologico è stato paradossalmente trasformato in una minaccia ambientale a causa dell'ipocrisia umana.</br>Con questo design vogliamo ricordare che dietro un bicchiere di plastica non c'è solo uno spreco ecologico, ma esiste soprattutto uno spreco di un'opportunità, di un'idea.");
    $('.modal img').attr('src', 'Wast_Bicchiere_Black.svg');
    showModal();
  });

  $('.button-origami').on('click', function () {
    $('.modal h2').html('Origami indovino');
    $('.modal p').html("Il pensiero di Jean-Paul Sartre, famoso filosofo e scrittore del '900, nasce dal concetto che un individuo sia il diretto responsabile delle proprie scelte. Ognuno di noi dovrebbe ragionare, pensare a dove porteranno le decisioni che prendiamo. O vogliamo lasciare tutto al caso?");
    $('.modal img').attr('src', 'OrigamiIndovino.png');
    showModal();
  });

  $('.button-impiccato').on('click', function () {
    $('.modal h2').html('Impiccato');
    $('.modal p').html("Chi non ha mai affrontato il gioco dell'impiccato da bambino? </br> E sicuramente vi ricordate quel ragazzo che tentava di indovinare la parola quando ancora gli spazi non erano riempiti, quasi per dimostrare la propria furbizia. </br> Allora perché dovremmo giocare su argomenti più grandi di noi? Soprattutto se al patibolo non c'è solamente un disegno.");
    $('.modal img').attr('src', 'Impiccato.svg');
    showModal();
  });

  $('.button-barchetta').on('click', function () {
    $('.modal h2').html('Haiku');
    // $('.modal p').html("Chi non ha mai affrontato il gioco dell'impiccato da bambino? </br> E sicuramente vi ricordate quel ragazzo che tentava di indovinare la parola quando ancora gli spazi non erano riempiti, quasi per dimostrare la propria furbizia. </br> Allora perché dovremmo giocare su argomenti più grandi di noi? Soprattutto se al patibolo non c'è solamente un disegno.");
    $('.modal img').attr('src', 'barchetta.svg');
    showModal();
  });

  $('.button-wastin').on('click', function () {
    $('.modal h2').html("I'm Wastin' It");
    // $('.modal p').html("Chi non ha mai affrontato il gioco dell'impiccato da bambino? </br> E sicuramente vi ricordate quel ragazzo che tentava di indovinare la parola quando ancora gli spazi non erano riempiti, quasi per dimostrare la propria furbizia. </br> Allora perché dovremmo giocare su argomenti più grandi di noi? Soprattutto se al patibolo non c'è solamente un disegno.");
    $('.modal img').attr('src', 'WastIt.svg');
    showModal();
  });

  function showModal() {
    $('.modal').css('opacity', '1').css('visibility', 'visible');
  }

  function hideModal() {
    $('.modal').css('opacity', '0').css('visibility', 'hidden');
  }

  function isScrolledIntoView($elem, $window, offset) {
      var docViewTop = $window.scrollTop();
      var docViewBottom = docViewTop + $window.height();

      var elemTop = $elem.offset().top;
      var elemBottom = elemTop + $elem.height();

      return ((elemBottom <= docViewBottom-offset) && (elemTop >= docViewTop));
  }

});
